chrome.runtime.onConnect.addListener(function (port) {
  // get the "word" from message
  console.assert(port.name === "transliterate");
  var disconnected = false;
  port.onDisconnect.addListener((p) => {
    disconnected = true;
  });
  port.postMessage({ "message": "connected" });

  port.onMessage.addListener(function (request) {
    if (disconnected == false) {
      port.postMessage({ message: "recieved",word: request.word});
      let word = request.word;      
      const Http = new XMLHttpRequest();
      const url = 'https://nayanapluginserver.pythonanywhere.com/naYana/IPA/getIpaHtml';
      Http.open("POST", url);
      let obj = { html: word ,lang: request.lang};
      obj = JSON.stringify(obj);
      Http.send(obj);     
      
      port.postMessage({ message: "sent" });

      Http.onreadystatechange = function () {

        if (Http.readyState === 4) {
          if (disconnected == false) {
            if (this.status == 200) {                        
              translatedText = Http.responseText;            
              
              port.postMessage({ "message": "replace", "replace": translatedText});
            }
            else {
              port.postMessage({ "message": "error" });
            }
          }
        }
      }
    }
  })

});

