[![Made withJupyter](https://img.shields.io/badge/Made%20with-Jupyter-orange?style=for-the-badge&logo=Jupyter)](https://jupyter.org/try)

# naYana Browser Plugin
naYana is a universal script which has been designed to achieve the aim of universal literacy. It is easy to learn phonetic script where learning to spell a word is made redundant. The script is created by Nagarjuna G. and Vickram Crishna and few other collaborators and interns at the gnowledge lab (https://www.gnowledge.org) of Homi Bhabha Centre for Science Education (https://www.hbcse.tifr.res.in). In order to know more about naYana and its features, feel free to check out [the official page of naYana](https://www.gnowledge.org/projects/naYana) at gnowledge lab.

## Why naYana?
There are presently hundreds of script in the world with the Latin alphabet being the most popular one. Why one should then shift to a new script like naYana and what's the need of it? Here are some of the features that make naYana the best choice among all the scripts in the world :
### No specific Text Directionality
As one might be aware that there is no specific order in which scripts are written. While Latin is written from left to right, Arabic is written from right to left. However naYana can be written from left to right, or from right to left, or top down, only the writer and the reader need to know the order. As a proposed universal alphabet, this could be a factor for universal adoption.
### Unique shapes and their transformations
No shape is reused by symmetric transformations (rotation or mirror forms) in naYana unless they are required semantically as in the case of brackets. This means that even if you rotate or take mirror image of the naYana script, all the mappings are still uniquely determined. Latin alphabets like 'W' and 'M' as one example, suffer from this anamoly. This unique feature ensures that no child is punished for their intuitive grasp of the shape while learning.
### Easy to Learn and Simple
naYana has much simpler and fewer glyphs than IPA which gives it an edge over it.
### Completely phonetic
Another advantage of naYana is that it is completly phonetic, since it is mapped directly to IPA symbols and sounds. This means a word is written exactly the way it is spoken and someone without even a hint of the language can read the text if it is present in naYana. It is a script where learning to spell a word is made redundant, and no spelling olympiads can be held, because of its phonetic nature.

## naYana alphabets
<div markdown="1">
<img src="./mapping/alphabets.png" width="70%"></img>
</div>
<h3> Vowels </h3>
<div markdown="1">
<img src="./mapping/vowels.png" width="70%"></img>
</div>
<h3> Consonants </h3>
<div markdown="1">
<img src="./mapping/consonants.png" width="100%"></img>
</div>

## How to install Browser Plugin
You can install **naYana Scribe** plugin on your browsers by following these steps.

**Note:** These steps are for Chrome users, however the plugin is compatible with Firefox as well and almost similar steps can be replicated to install the plugin on Firefox.

1. Go to the **[extensions directory](https://gitlab.com/gnowledge/nayana_browser_plugin/-/tree/master/extension)** in the repository.

![Extensions directory screenshot](./images/Screenshot__65_.png "Extensions directory")

2. Click on the **download icon**.

![Download Icon Screenshot](./images/Screenshot__66_.png "Download icon")

3. Download the **zip file** for the extensions directory.

![Download Zip Screenshot](./images/Screenshot__67_.png "Download zip directory")

4. Save the **zip file** on your system and **extract the extensions folde**r to a suitable place.

![Zip File Screenshot](./images/Screenshot__68_.png "Zip File")

5. Now click on the **Extensions icon** in the top right corner of your browser window.

![Extensions Icon Screenshot](./images/Screenshot__69_.png "Extension Icon")

6. Click on **Manage Extensions**.

![Manage Extensions Screenshot](./images/Screenshot__70_.png "Manage Extensions")

7. Click on **Load Unpacked**. 

![Load Unpacked Screenshot](./images/Screenshot__71_.png "Load Unpacked")

8. Browse to the directory containing the **extensions folder** and select it.

![Browse to extensions folder Screenshot](./images/Screenshot__72_.png "Browse to extensions folder")

9. The **naYanaScribe plugin is installed** on your browser.

![Completion Screenshot](./images/Screenshot__73_.png "Completion")






