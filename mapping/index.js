var i = 0;
var txt = 'Type in naYana'; /* The text */
var speed = 50; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
    if (i < txt.length) {
        document.getElementById("menu_id").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}
var isChrome = navigator.userAgent.includes('Chrome');
var isFirefox = navigator.userAgent.includes('Firefox');
console.log(isChrome);
console.log(isFirefox);
document.addEventListener('DOMContentLoaded', function () {
    typeWriter();
    if (isChrome) {
        var inp = document.getElementById("inp");
        var IPA = document.getElementById("IPA");
        var nYn = document.getElementById("nYn");
        const temp = [inp, IPA, nYn];

        // Apply CSS property to it
        for (var i = 0; i < temp.length; i++) {
            temp[i].style.whiteSpace = "pre-wrap";
        }
    }
    if (isFirefox) {
        var inp = document.getElementById("inp");
        var IPA = document.getElementById("IPA");
        var nYn = document.getElementById("nYn");
        const temp = [inp, IPA, nYn];

        // Apply CSS property to it
        for (var i = 0; i < temp.length; i++) {
            temp[i].style.whiteSpace = "-moz-pre-wrap;";
        }
    }
    document.getElementById('warning').style.visibility = 'hidden';
    document.getElementById('warning').style.position = 'absolute';

});
var log_id = 0;

function preventEntry() {//editing IPA and naYana parts not allowed
    event.preventDefault();
}

function showWarning(inp, key) {
    if (key.length === 1) {
        inp.classList.add('error');
        setTimeout(function () { inp.classList.remove('error') }, 500);
        document.getElementById('warning').style.visibility = 'visible';
        document.getElementById('warning').style.position = 'relative';
    }
}

function clearElement(id) {//clears the text Content of an element with input id
    ele = document.getElementById(id);
    try {
        ele.textContent = "";
        ele.value = ""
    }
    catch (err) { }
}
function logIntoElement(id_from, id_to, isFromTextarea, isToTextArea) {//logs the textContent from one element to another and clears the from element
    let ele_from = document.getElementById(id_from);
    console.log("ele from:", ele_from);
    let ele_to = document.getElementById(id_to);
    console.log("ele_to", ele_to);
    var content;
    if (isFromTextarea) {
        content = ele_from.value;
    }
    else {
        content = ele_from.textContent;
    }
    if (isToTextArea) {
        ele_to.value = content;
    }
    else {
        ele_to.textContent = content;
    }

    clearElement(id_from);
}
function copyToClipboard(id) {//copy textContent of an element to clipboard        
    var to_copy = document.getElementById(id);
    var copy_text = document.getElementById("copy");
    copy_text.value = to_copy.textContent;
    console.log(to_copy);
    copy_text.select();
    copy_text.setSelectionRange(0, 99999);
    document.execCommand('copy');
    return;
}
function uniKeyCode(event) {
    let dict = {
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "5": "5",
        "6": "6",
        "7": "7",
        "8": "8",
        "9": "9",
        "0": "0",

        ".": ".",
        " ": " ",
        "*": "*",
        "।": "।",
        ",": ",",

        'a': 'a',
        'b': 'b',
        'c': 'ʧ',
        'd': 'ð',
        'e': 'ɛ',
        'f': 'f',
        'g': 'ɡ',
        'h': 'h',
        'i': 'ɪ',
        'j': 'ʤ',
        'k': 'k',
        "l": "l",
        "m": "m",
        "n": "n",
        "o": "o",
        "p": "p",
        'q': 'q',
        'r': ':',
        's': 's',
        't': 'ʈ',
        'u': 'ʊ',
        'v': 'v',
        'w': 'w',
        'z': 'z',

        'A': 'æ',
        'D': 'd',
        'E': 'e',
        // 'H': 'ʰ',
        'N': 'ɳ',
        'R': 'r',
        'S': 'ʃ',
        'T': 't',
        'Y': 'j',
        'O': 'ɔ'
    };
    let dict_h = {
        'ɽ': 'ɽ̥',   //"D"+"H"
        'z': 'ʒ',   //"z"+"H"
        't': 'θ',   //"T"+"H"
        'ʃ': 'ʂ'    //"S"+"H"
    };
    let dict_alt = {    //_ is altKey here
        'n': 'ŋ',    //"n_"
        'g': 'ɲ',    //"g_"
        'r': 'r̩',    //"r_"
        'd': 'ɽ'     //"d_"
    };
    var key = event.key;
    var keycode = event.which;
    let input = document.getElementById("inp");
    let IPA = document.getElementById("IPA");
    let nYn = document.getElementById("nYn");
    document.getElementById('warning').style.visibility = 'hidden';
    document.getElementById('warning').style.position = 'absolute';

    if (event.ctrlKey) {
        if (key === 'c' || key === 'C') {
            return;
        }
        if (key === 'l' || key === 'L') {
            createLogEntry();
            return;
        }
    }
    event.preventDefault();
    if (!event.ctrlKey) {
        if (!event.altKey) {
            if (key in dict) {
                if (input.selectionStart === input.value.length) {//ensure caret is at end of input string
                    input.value = input.value + key;
                    IPA.textContent += dict[key];
                    nYn.textContent = IPA.textContent;
                }
            }
            else if (key === 'H') {
                let str_IPA = IPA.textContent;
                let len_IPA = str_IPA.length;

                if (input.selectionStart === input.value.length) {//ensure caret is at end of input string
                    input.value += "H";
                    if (len_IPA !== 0 && str_IPA.charAt(len_IPA - 1) in dict_h) { //short circuit
                        let last_char = dict_h[str_IPA.charAt(len_IPA - 1)];
                        str_IPA = str_IPA.substring(0, len_IPA - 1);
                        IPA.textContent = str_IPA + last_char;
                        nYn.textContent = IPA.textContent;
                    }
                    else {
                        IPA.textContent = IPA.textContent + 'ʰ';
                        nYn.textContent = IPA.textContent;
                    }
                }
            }
            else if (keycode == 8) { //To allow removal of characters using backspace in IPA and nYn column
                let str_input = input.value;
                let len_input = str_input.length;
                if (input.selectionStart === input.value.length) {
                    if (len_input != 0) {
                        let last = str_input.charAt(len_input - 1);
                        let second_last = str_input.charAt(len_input - 2);
                        let third_last = str_input.charAt(len_input - 3);
                        input.value = str_input.substring(0, input.value.length - 1);
                        if (last === 'H') {
                            if (dict[second_last] in dict_h || (dict_alt[second_last] in dict_h) && third_last === '↑') {
                                if (dict_alt[second_last] in dict_h) {
                                    IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - dict_h[dict_alt[second_last]].length);
                                    IPA.textContent += dict_alt[second_last];
                                    nYn.textContent = IPA.textContent;
                                }
                                else {
                                    IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - dict_h[dict[second_last]].length);
                                    console.log(IPA.textContent);
                                    IPA.textContent += dict[second_last];
                                    nYn.textContent = IPA.textContent;
                                }
                            }
                            else {
                                IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - 1);
                                nYn.textContent = IPA.textContent;
                            }
                        }
                        else if (second_last === '↑') {
                            input.value = str_input.substring(0, input.value.length - 1);
                            IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - dict_alt[last].length);
                            nYn.textContent = IPA.textContent;
                        }
                        else if (last === '\n') {
                            IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - 2);
                        }
                        else {
                            IPA.textContent = IPA.textContent.substring(0, IPA.textContent.length - 1);
                            nYn.textContent = IPA.textContent;
                        }

                    }

                }
            }
            else if (keycode === 13) {
                input.value += "\n";
                IPA.textContent += "\r\n";
            }
            else {
                console.log(key, "in first if");
                showWarning(inp, key)
            }

        }


        else if (key in dict_alt) {
            if (input.selectionStart === input.value.length) {
                input.value = input.value + '↑' + key;
                IPA.textContent += dict_alt[key];
                nYn.textContent = IPA.textContent;
            }
        }
        else {
            showWarning(inp, key);
        }

    }

    else {
        showWarning(inp, key);
    }



}
function createLogEntry() {
    if (document.getElementById("inp").value.length === 0) { return; }
    log_id += 1;
    let newrow = "<div id='" + 'row' + log_id + "' class='row logrow'></div>";
    let newinpLog = "<div id='" + 'inp' + log_id + "div'class='logcolumn' ><span class='logspan inpcolumn' id='" + 'inp' + log_id + "'></span></div>";
    let newIpaLog = "<div id='" + 'IPA' + log_id + "div'class='logcolumn IPAlogcolumn' ><span class='logspan IPAcolumn' id='" + 'IPA' + log_id + "'></span></div>";
    let newnYnLog = "<div id='" + 'nYn' + log_id + "div'class='logcolumn' ><span class='logspan nYncolumn' id='" + 'nYn' + log_id + "'></span></div>";
    let log = document.getElementById("log");

    log.innerHTML += newrow;
    document.getElementById('row' + log_id).innerHTML += newinpLog + newIpaLog + newnYnLog;

    console.log(log.innerHTML);
    logIntoElement("inp", "inp" + log_id, true, false);
    logIntoElement("IPA", "IPA" + log_id, false, false);
    logIntoElement("nYn", "nYn" + log_id, false, false);
    let IPA_id = "IPA" + log_id + 'div';
    let inp_id = "inp" + log_id + 'div';
    let nYn_id = "nYn" + log_id + 'div';
    document.getElementById(IPA_id).innerHTML += "<button class='copybutton' type='button' onclick='copyToClipboard(" + '"' + IPA_id + '"' + ")'></button>";
    let height_IPA = document.getElementById(IPA_id).offsetHeight;
    let height_nYn = document.getElementById(nYn_id).offsetHeight;
    let height = height_nYn;
    if (height_IPA >= height_nYn) {
        height = height_IPA;
    }
    document.getElementById(inp_id).style.height = height;
    document.getElementById(nYn_id).style.height = height;

    var inp_logspan = document.getElementById("inp" + log_id);
    var IPA_logspan = document.getElementById("IPA" + log_id);
    var nYn_logspan = document.getElementById("nYn" + log_id);
    const temp = [inp_logspan, IPA_logspan, nYn_logspan];
    console.log(isChrome);
    if (isChrome) {
        // Apply CSS property to it
        for (var i = 0; i < temp.length; i++) {
            temp[i].style.whiteSpace = "pre-wrap";
        }
    }
    if (isFirefox) {
        // Apply CSS property to it
        for (var i = 0; i < temp.length; i++) {
            temp[i].style.whiteSpace = "-moz-pre-wrap;";
        }
    }
}

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    document.getElementById("Con").style.left = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("Con").style.left = "-54px";
}
function openNav2() {
    document.getElementById("mySidenav2").style.width = "100%";
    document.getElementById("Vow").style.left = "100%";
}

function closeNav2() {
    document.getElementById("mySidenav2").style.width = "0";
    document.getElementById("Vow").style.left = "-32px";
}
