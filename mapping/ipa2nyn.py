# IPA TO NAYANA MAPPING
dict_i2n = {
    # Numbers
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",
    "0": "0",
   
    # Symbol for stressing on a phoneme not needed in Nayana
    "ˈ": "",
    "ˌ": "",    

    # Word separators (basic)
    ".": ".",
    " ": " ",
    "*": "*",
    "।": "।",
    ",": ",",

    # One to One Mapping
    "ʌ": "a",
    "ə": "a",
    "a": "a",
    "b": "b",
    "ʧ": "c",
    "ð": "d",
    "ɛ": "e",
    "f": "f",
    "g": "g",
    "ɡ": "g",
    "h": "h",
    "ɦ": "h",
    "ɪ": "i",
    "i": "i",
    "ʤ": "j",
    "k": "k",
    "l": "l",
    "m": "m",
    "n": "n",
    "o": "o",
    "p": "p",
    ":": "r", #spacer
    "s": "s",
    't': 'T', #(त)
    "ʈ": "t", #(ट)
    "ʊ": "u",
    "v": "v",
    "w": "w",
    "z": "z",
    'q' :'q',    
    "æ": "A",
    "e": "E",
    "ɳ": "N",
    "r": "R",    
    "d": "D",
    "ɖ": "D",
    "j": "Y",
    "ʃ": "S",    
    #         '?' : 'x',  (reserved for localization)
    #         '?' : 'L'  (reserved for localization)
    'ʰ': "h",
    '̤': "h",     
     
    # Sounds made by combination of naYana alphabets
    "ɔ": "au",  # awe sound (halt)
    "ŋ": "ng",  # ng sound (song)
    "ɲ": "n",   # gn as in (align)
    "u": "ur",  # oo sound (cool)
    "ɑ": "ar",  # aa sound (pasta/father)

    #'i' : 'ii', #prolonged e sound (meet/neat) --different for english and hindi also t

    "ʂ": "S_", #(ष) or Sh 
    "θ": "Th",
    "r̩": "Ri",
    "ʒ": "zh",
    'ɖ̤': 'Dh', # ढ   
    'ɽ': 'D_', # (ड़) 
    'ɽ̥': 'D_h', #(ढ़)
    
 # Multiple IPA to single naYana character (will be done through ligature) the group above also has to be handled through ligature
    #           'əɪ': 'E'
    #           'aɪ': 'y',
    #           'nd': 'N',
    #           "pʰ": "f",  
    #           'kʰ': 'kh',
    #           'ɡʰ':'gh',
    #           't͡ʃʰ':'ch',  #(छ)
    #           'dʒʰ': 'jh',
    #           'ʈʰ': 'th', #(ठ)
    #           'd̤': 'dh',
    #           'b̤' : 'bh',
    #           'tʰ': 'Th'   

}

