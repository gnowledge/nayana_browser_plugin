function legitOrNot(str) {
    return Boolean(str.trim());

}
var original_body = document.body.innerHTML;
var port = null;
var orig_document;
var nyn_document = null;
var toggle;
let originalFamily = document.body.style.fontFamily;
let previousRange = null;
let previousText = "";
function replaceSelectedText(replacementText, fontFamily) {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            previousRange = range;
            range.deleteContents();
            let sp = document.createElement('span');
            sp.style.fontFamily = 'naYanakamik';
            sp.innerText = replacementText;
            range.insertNode(sp);
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.text = replacementText;
    }
}
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.message === "convertSelection") {
        var style = document.createElement('style');
        style.innerHTML = `
    @font-face {
			font-family: 'naYanakamikRegular';
			src: url("chrome-extension://${chrome.runtime.id}/assets/naYanakamikRegularFinal.otf") format("opentype");
		}`;
        document.head.appendChild(style);
        if (previousRange) {
            previousRange.deleteContents();
            let sp = document.createElement('span');
            sp.style.fontFamily = originalFamily;
            sp.innerText = previousText;
            previousRange.insertNode(sp);
        }
        let selection = document.getSelection();
        selection = selection.toString();
        previousText = selection;
        if (selection == "") previousRange = null;
        console.log(selection);
        if (port == null) {
            port = chrome.runtime.connect({ name: "transliterate" });
        }
        port.postMessage({ "toConvert": selection, "lang": request.lang });
        port.onMessage.addListener(function (request_port) {
            if (request_port.message == 'selectionResult') {
                console.log(request_port.result);
                replaceSelectedText(request_port.result);
            }
            if (request_port.message == "SelectionSent") {
                console.log(request_port.message);
            }
            if (request_port.message == "connected") { console.log('connected'); }
        }
        )
    }

    if (request.message === "getLang") {
        let html_tag = document.getElementsByTagName("HTML")[0];
        let lang = html_tag.getAttribute('lang');
        // chrome.runtime.sendMessage({lang});
        sendResponse({ lang });
    }
    if (request.message === "reverse") {
        toggle = false;
        document.body.outerHTML = orig_document.body.outerHTML;
    }
    if (request.message === "translate") {
        toggle = true;
        if (nyn_document == null) {
            original_body = document.body.outerHTML;
            orig_document = document.cloneNode(true);
            nyn_document = document.cloneNode(true);
            console.log("translate")
            //     const nynfont = new FontFace('nayana', 'url(https://nayanapluginserver.pythonanywhere.com/naYana/IPA/fontFile)');
            //     nynfont.load();
            //     document.fonts.add(nynfont);

            var style = nyn_document.createElement('style');
            style.innerHTML = `
    @font-face {
			font-family: 'naYanakamikRegular';
			src: url("chrome-extension://${chrome.runtime.id}/assets/naYanakamikRegularFinal.otf") format("opentype");
		}`;
            document.head.appendChild(style);
            nyn_document.body.style.fontFamily = 'naYanakamikRegular';
            nyn_document.body.style.setProperty('font-family', 'naYanakamikRegular', 'important');
            let lang = request.lang;
            port = chrome.runtime.connect({ name: "transliterate" });
            port.onMessage.addListener(function (request_port) {
                if (request_port.message === "replace") {
                    idx = request_port.idx;
                    replace = request_port.replace;
                    success = request_port.success;
                    console.log(replace);
                    for (var i = 0; i < idx.length; i++) {
                        if (success[i] == 1) {
                            let cur_node = fetchElementWithIndex(nyn_document.body, idx[i]);
                            cur_node.nodeValue = replace[i];
                        }
                    }
                    console.log(nyn_document);
                    if (toggle == true) {
                        document.body.outerHTML = nyn_document.body.outerHTML;
                    }
                }
                else if (request_port.message === "error") {
                    console.log("Sorry some error happened :(");
                }
                else if (request_port.message === "connected") {
                    console.log("connected");
                }                
                else if (request_port.message === "sent") {
                    console.log("sent to api");
                }

            });
            iterateThroughTree(nyn_document.body, port, lang);
        }
        else {
            document.body.outerHTML = nyn_document.body.outerHTML;
        }
    }

});

function iterateThroughTree(element, port, lang) {
    let idx = [];
    last_operation = "down";
    let next = element;

    while (next !== null) {
        switch (last_operation) {
            case "down":
                idx.push(0);
                break;
            case "up":
                idx.pop();
                break;
            case "right":
                let i = idx.pop();
                idx.push(i + 1);
                break;
        }
        if (next.hasChildNodes() && last_operation !== "up" && next.tagName != "SCRIPT" && next.tagName != "STYLE") {
            last_operation = "down";
            next = next.firstChild;
        }
        else {
            let temp_next = next;
            if (next.nextSibling != null) {
                last_operation = "right";
                temp_next = next.nextSibling;
            }
            else {
                last_operation = "up";
                temp_next = next.parentNode;
            }
            if (next.nodeType === 3) {
                // console.log(Boolean(temp_next==null));
                getTransliteration(next.nodeValue, idx.join('_'), Boolean(temp_next === null), port, lang);
                // chrome.runtime.sendMessage({ "word": next.textContent, "idx": idx.join("_") });
            }
            next = temp_next;
            if (Boolean(temp_next == null)) {
                getTransliteration("", "", true, port, lang);
            };
        }
    }
}

function fetchElementWithIndex(element, idx) {
    let cur = element;
    itr = idx.split("_");
    for (let x = 0; x < itr.length; x++) {
        itr[x] = parseInt(itr[x]);
    }
    itr.shift();
    console.log(itr);
    for (let x = 0; x < itr.length; x++) {
        if (cur.childNodes[itr[x]] == undefined) {
            console.log("undefined here");
            console.log(cur);
        }
        else {
            cur = cur.childNodes[itr[x]];
        }
    }
    return cur;
}
var toConvert_arr = [];
var idx_arr = [];
var batch_size = 10;
function getTransliteration(str, idx, over, port, lang) {
    if (over) {
        console.log("over");
        port.postMessage({ "word": toConvert_arr, "idx": idx_arr, "lang": lang });
        console.log("sent the last batch");
        toConvert_arr = [];
        idx_arr = [];
    }
    if (legitOrNot(str)) {
        toConvert_arr.push(str);
        idx_arr.push(idx);
        if (toConvert_arr.length >= batch_size) {
            port.postMessage({ "word": toConvert_arr, "idx": idx_arr, "lang": lang });
            console.log("sent a batch")
            toConvert_arr = [];
            idx_arr = [];
        }
    }
}