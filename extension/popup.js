let lang = "en";
let toggle = false;
document.addEventListener('DOMContentLoaded', function() {
  const Inp = document.getElementById('ToggleIn');
  const selBtn=document.getElementById('SelectTranslate');
  selBtn.addEventListener('click',function(){
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(tabs[0].id, { "message": "convertSelection","lang":lang});
    });
  });
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    let key = tabs[0].id.toString()+"toggle";
    
    chrome.storage.local.get([`${key}`],function(data) {
      if(data[key]!=undefined){
        toggle = data[key];
        if(toggle==true){
          Inp.checked = true;
        }
      }
    });
    chrome.tabs.sendMessage(tabs[0].id, {"message": "getLang"}, function(response){
      const sel=document.getElementById("lang");
      if(response.lang){
        if(response.lang.substr(0,2) == "hi"){
          lang="hi";
          sel.value = 'hi';
        }
        else sel.value = 'en';
      }
      else{
        sel.value = 'en';
      }
    })  
  });
  
    const toggl = document.getElementById('ToggleIn');
   
    const sel=document.getElementById("lang");
    sel.addEventListener('change',function(){
      lang=sel.value;
      console.log(lang);
    });
    toggl.addEventListener('click', function() {
      if(toggle===false){
        toggle=true;
        Inp.checked = true;
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
          let key = tabs[0].id +"toggle";
          chrome.storage.local.set({[key]: toggle},function(){
            console.log("value is set to"+toggle);
          });
          chrome.tabs.sendMessage(tabs[0].id, {"message": "translate", "lang": lang});
        });
      }
      else{
        toggle=false;
        Inp.checked = false;
        console.log(toggle);
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
          let key = tabs[0].id +"toggle";
          chrome.storage.local.set({[key]: toggle},function(){
            console.log("value is set to"+toggle);
          });
          chrome.tabs.sendMessage(tabs[0].id, {"message": "reverse", "lang": lang});
        });
      }
    });
  });
