chrome.runtime.onConnect.addListener(function (port) {
  // get the "word" from message
  console.assert(port.name === "transliterate");
  var disconnected = false;
  port.onDisconnect.addListener((p) => {
    disconnected = true;
  });
  port.postMessage({ "message": "connected" });

  port.onMessage.addListener(function (request) {
    if (disconnected == false) {
      port.postMessage({ message: "recieved" });
      if (request.word) {
        let word = request.word;
        let idx = request.idx;
        const Http = new XMLHttpRequest();
        const url = 'https://nayanapluginserver.pythonanywhere.com/naYana/IPA/getIpaDetect';
        Http.open("POST", url);
        let obj = { str: word, lang: request.lang };
        obj = JSON.stringify(obj);
        Http.send(obj);
        port.postMessage({ message: "sent" });
        port.onDisconnect.addListener((p) => {
          Http.removeEventListener('readystatechange', handleHttpStatechange, true);
        })
        function handleHttpStatechange(event) {

          if (Http.readyState === 4) {
            if (this.status == 200) {
              console.log(Http.responseText);
              let obj = JSON.parse(Http.responseText);

              translatedText = obj.IPA;
              success = obj.success;

              port.postMessage({ "message": "replace", "replace": translatedText, "idx": idx, "success": success });
            }
            else {

              port.postMessage({ "message": "error" });
            }
          }
        }
        Http.addEventListener('readystatechange', handleHttpStatechange, true);
      }
      else if (request.toConvert) {
        const Http = new XMLHttpRequest();
        const url = 'https://nayanapluginserver.pythonanywhere.com/naYana/IPA/getIpaDetect';
        Http.open("POST", url);
        let obj = { "str": [request.toConvert], "lang": request.lang };
        obj = JSON.stringify(obj);
        Http.send(obj);
        port.postMessage({ message: "SelectionSent" });
        port.onDisconnect.addListener((p) => {
          Http.removeEventListener('readystatechange', handleHttpStatechangeselect, true);
        })
        function handleHttpStatechangeselect(event) {

          if (Http.readyState === 4) {
            if (this.status == 200) {
              console.log(Http.responseText);
              let obj = JSON.parse(Http.responseText);

              translatedText = obj.IPA;
              success = obj.success;

              port.postMessage({ "message": "selectionResult", "result": translatedText, "success": success });
            }

          }
        }
        Http.addEventListener('readystatechange', handleHttpStatechangeselect, true);
      }
    }
  })

});

