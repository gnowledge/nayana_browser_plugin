nayanakeyboard keyboard
==============

© Gnowledge Lab

Version 1.0

Links
-----
https://www.gnowledge.org/projects/naYana

Usage Instructions
-------------------
1. Download and Install [Keyman](https://keyman.com/downloads/) 
2. Navigate to [Build Folder](https://gitlab.com/gnowledge/nayana_browser_plugin/-/tree/master/nayanakeyboard/build) and Install nayanakeyboard.kmx file on keyman.

Supported Platforms
-------------------
 * Windows
 * macOS
 * Linux
 * Web
 * iPhone
 * iPad
 * Android phone
 * Android tablet
 * Mobile devices
 * Desktop devices
 * Tablet devices

